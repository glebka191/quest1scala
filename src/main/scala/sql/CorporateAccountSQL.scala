package sql

import org.apache.spark.sql.{DataFrame, SparkSession}
import showcases.CalculationParams

class CorporateAccountSQL (operationsDf:DataFrame, accounts:DataFrame,
                           clients:DataFrame, currency:DataFrame, spark:SparkSession){
  /*
  val calc = new CalculationParams(spark).calculationParams
  accounts.createOrReplaceTempView("account")
  clients.createOrReplaceTempView("client")
  operationsDf.createOrReplaceTempView("operationDF")
  currency.createOrReplaceTempView("rate")

  private val operationPayments = new OperationPaymentsSQL(operationsDf, accounts,
    clients, currency, spark, calc)

  private[sql] val actualRate: DataFrame = operationPayments.actualRate
  private[sql] val operation: DataFrame = operationPayments.operation
  private[sql] val operations: DataFrame = operationPayments.operations
  def getCorporateAccount: DataFrame = {
    totalAmt.createOrReplaceTempView("TotalAmt")
    spark.sql("""
        SELECT a.AccountId, a.AccountNum, a.DateOpen, c.ClientId, c.ClientName, ta.TotalAmt, ta.CutoffDt
        FROM account a
        LEFT JOIN client c ON a.ClientId = c.ClientId
        LEFT JOIN TotalAmt ta ON ta.AccountId = a.AccountId
        ORDER BY a.AccountId, ta.CutoffDt
        """)
  }

  /*TotalAmt – Общая сумма оборотов по счету. Считается как сумма PaymentAmt и EnrollementAmt*/
  private[sql] def totalAmt: DataFrame = {
    operations.createOrReplaceTempView("operations")
    paymentAmt.createOrReplaceTempView("PaymentAmt")
    enrollementAmt.createOrReplaceTempView("EnrollementAmt")

    spark
      .sql(
        """
        SELECT o.AccountId, (PaymentAmt + EnrollementAmt) as TotalAmt, o.CutoffDt
        FROM operations o
        LEFT JOIN PaymentAmt pa ON o.AccountId = pa.AccountId and o.CutoffDt = pa.CutoffDt
        LEFT JOIN EnrollementAmt ea ON o.AccountId = ea.AccountId and o.CutoffDt = ea.CutoffDt"""
      )
  }

  /*PaymentAmt – сумма операций по счету, где счет клиента указан в дебете проводки (AccountDB)*/
  private def paymentAmt: DataFrame = {
    spark
      .sql(
        "SELECT o.AccountId, CASE WHEN p.PaymentAmt is null THEN 0 ELSE p.PaymentAmt END as PaymentAmt, o.CutoffDt " +
          "from operations o LEFT JOIN " +
          "(SELECT AccountDB as AccountId, CutoffDt, sum(Amount) as PaymentAmt " +
          "FROM operation " +
          "GROUP BY AccountDB, CutoffDt) p " +
          "ON o.AccountId = p.AccountId and o.CutoffDt = p.CutoffDt"
      )
  }

  /*EnrollementAmt – сумма операций по счету, где счет клиента указан в  кредите проводки (AccountCR)*/
  private def enrollementAmt: DataFrame = {
    spark
      .sql(
        "SELECT o.AccountId, CASE WHEN p.EnrollementAmt is null THEN 0 ELSE p.EnrollementAmt END as EnrollementAmt, o.CutoffDt " +
          "from operations o " +
          "LEFT JOIN (" +
          "SELECT AccountCR as AccountId, CutoffDt, sum(Amount) as EnrollementAmt " +
          "FROM operation " +
          "GROUP BY AccountCR, CutoffDt" +
          ") p " +
          "ON o.AccountId = p.AccountId and o.CutoffDt = p.CutoffDt"
      )
  }

   */
}
