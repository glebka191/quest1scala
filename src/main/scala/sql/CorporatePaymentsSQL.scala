package sql

import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.storage.StorageLevel
import showcases.CalculationParams

class CorporatePaymentsSQL(operationsDf:DataFrame, accounts:DataFrame,
                           clients:DataFrame, currency:DataFrame, spark:SparkSession, masks:DataFrame) {

  val calcParam = new CalculationParams(spark)

  accounts.createOrReplaceTempView("account")
  clients.createOrReplaceTempView("client")
  operationsDf.createOrReplaceTempView("operationDF")
  currency.createOrReplaceTempView("rate")

  def getOperationPayments: DataFrame = {
    actualRate.createOrReplaceTempView("actualRate")
    operation.createOrReplaceTempView("operation")
    operations.createOrReplaceTempView("operations")
    operation_payments.createOrReplaceTempView("operation_payments")
    paymentAmt.createOrReplaceTempView("PaymentAmt")
    enrollementAmt.createOrReplaceTempView("EnrollementAmt")
    taxAmt.createOrReplaceTempView("TaxAmt")
    clearAmt.createOrReplaceTempView("ClearAmt")
    carsAmt.createOrReplaceTempView("CarsAmt")
    foodAmt.createOrReplaceTempView("FoodAmt")
    flAmt.createOrReplaceTempView("FLAmt")

    spark.sql("""
        SELECT op.ClientId, op.AccountId, pa.PaymentAmt, ea.EnrollementAmt, tax.TaxAmt, clear.ClearAmt,
        cars.CarsAmt, food.FoodAmt, fl.FLAmt, op.CutoffDt
        FROM operation_payments op
        LEFT JOIN PaymentAmt pa ON op.AccountId = pa.AccountId and op.CutoffDt = pa.CutoffDt
        LEFT JOIN EnrollementAmt ea ON op.AccountId = ea.AccountId and op.CutoffDt = ea.CutoffDt
        LEFT JOIN TaxAmt tax ON op.AccountId = tax.AccountId and op.CutoffDt = tax.CutoffDt
        LEFT JOIN ClearAmt clear ON op.AccountId = clear.AccountId and op.CutoffDt = clear.CutoffDt
        LEFT JOIN CarsAmt cars ON op.AccountId = cars.AccountId and op.CutoffDt = cars.CutoffDt
        LEFT JOIN FoodAmt food ON op.AccountId = food.AccountId and op.CutoffDt = food.CutoffDt
        LEFT JOIN FLAmt fl ON op.AccountId = fl.AccountId and op.CutoffDt = fl.CutoffDt
        ORDER BY op.ClientId, op.CutoffDt
        """)
  }

  private[sql] def actualRate: DataFrame = {
    spark
      .sql(
        "SELECT Currency, Rate FROM rate " +
          "WHERE rateDate in (SELECT max(rateDate) FROM rate)"
      )
  }

  private[sql] def operation: DataFrame = {
    spark
      .sql(
        """
           SELECT AccountDB, AccountCR, DateOp as CutoffDt, (o.Amount*ar.Rate) as Amount, Comment
           FROM operationDF o
           INNER JOIN actualRate ar ON o.Currency = ar.Currency
          """
      )
      .persist(StorageLevel.MEMORY_ONLY)
  }

  private[sql] def operations: DataFrame = {
    spark
      .sql(
        "SELECT AccountCR as AccountId, CutoffDt " +
          "FROM operation " +
          "UNION (SELECT AccountDB, CutoffDt FROM operation)"
      )
  }

  private def operation_payments: DataFrame = {
    spark
      .sql(
        "SELECT ClientId, o.AccountId, CutoffDt " +
          "FROM account a " +
          "LEFT JOIN operations o " +
          "ON a.AccountId = o.AccountId"
      )
  }

  /*PaymentAmt – сумма операций по счету, где счет клиента указан в дебете проводки (AccountDB)*/
  private[sql] def paymentAmt: DataFrame = {
    operations.createOrReplaceTempView("operations")
    operation.createOrReplaceTempView("operation")
    spark
      .sql(
        "SELECT o.AccountId, p.PaymentAmt, o.CutoffDt " +
          "from operations o LEFT JOIN " +
          "(SELECT AccountDB as AccountId, CutoffDt, sum(Amount) as PaymentAmt " +
          "FROM operation " +
          "GROUP BY AccountDB, CutoffDt) p " +
          "ON o.AccountId = p.AccountId and o.CutoffDt = p.CutoffDt"
      )
  }

  /*EnrollementAmt – сумма операций по счету, где счет клиента указан в  кредите проводки (AccountCR)*/
  private[sql] def enrollementAmt: DataFrame = {
    spark
      .sql(
        "SELECT o.AccountId, p.EnrollementAmt, o.CutoffDt " +
          "from operations o " +
          "LEFT JOIN (" +
          "SELECT AccountCR as AccountId, CutoffDt, sum(Amount) as EnrollementAmt " +
          "FROM operation " +
          "GROUP BY AccountCR, CutoffDt" +
          ") p " +
          "ON o.AccountId = p.AccountId and o.CutoffDt = p.CutoffDt"
      )
  }

  /*TaxAmt – сумму операций, где счет клиента указан в дебете, и счет кредита 40702 (AccountNum)*/
  private def taxAmt: DataFrame = {
    spark
      .sql(
        "SELECT o.AccountId, p.TaxAmt, o.CutoffDt " +
          "FROM operations o " +
          "LEFT JOIN (" +
          "SELECT AccountDB as AccountId, CutoffDt, sum(Amount) as TaxAmt " +
          "FROM operation " +
          "WHERE AccountCR in (SELECT AccountId FROM account where AccountNum like (\"40702%\"))" +
          "GROUP BY AccountId, CutoffDt" +
          ") p " +
          "ON o.AccountId = p.AccountId and o.CutoffDt = p.CutoffDt"
      )
  }

  /*ClearAmt – сумма операций, где счет клиента указан в кредите, и счет дебета 40802 (AccountNum)*/
  private def clearAmt: DataFrame = {
    spark
      .sql(
        "SELECT o.AccountId, p.ClearAmt, o.CutoffDt " +
          "FROM operations o " +
          "LEFT JOIN (" +
          "SELECT AccountCR as AccountId, CutoffDt, sum(Amount) as ClearAmt " +
          "FROM operation " +
          "WHERE AccountDB in (SELECT AccountId FROM account where AccountNum like (\"40802%\")) " +
          "GROUP BY AccountId, CutoffDt" +
          ") p " +
          "ON o.AccountId = p.AccountId and o.CutoffDt = p.CutoffDt"
      )
  }

  /*CarsAmt – сумма операций, где счет клиента указан в дебете проводки и назначение платежа не содержит слов по маскам Списка 1 (Comment)*/
  private def carsAmt: DataFrame = {
    val filterCarsWords: String = s"""Comment NOT like "${calcParam
      .getWordsMasks(masks, 0)
      .mkString(
        """" and Comment NOT like """"
      )}""""
    spark
      .sql(
        s"""
          SELECT o.AccountId, ca.CarsAmt, o.CutoffDt
          FROM operations o LEFT JOIN
          (SELECT AccountDB as AccountId, sum(Amount) as CarsAmt, CutoffDt
            FROM operation
            WHERE ${filterCarsWords}
            GROUP BY AccountId, CutoffDt
          ) ca
        ON o.AccountId = ca.AccountId and o.CutoffDt = ca.CutoffDt"""
      )
  }

  /*FoodAmt – сумма операций, где счет клиента указан в кредите проводки и назначение платежа содержит слова по Маскам Списка 2 (Comment)*/
  private def foodAmt: DataFrame = {
    val filterFoodWords: String = s"""Comment like "${calcParam
      .getWordsMasks(masks, 1)
      .mkString(
        """" or Comment like """"
      )}""""
    spark
      .sql(
        s"""
          SELECT o.AccountId, fa.FoodAmt, o.CutoffDt
          FROM operations o LEFT JOIN
          (SELECT AccountCR as AccountId, sum(Amount) as FoodAmt, CutoffDt
            FROM operation
            WHERE ${filterFoodWords}
            GROUP BY AccountId, CutoffDt
          ) fa
        ON o.AccountId = fa.AccountId and o.CutoffDt = fa.CutoffDt"""
      )
  }

  /*FLAmt – сумма операций с физ. лицами. Счет клиента указан в дебете проводки, а клиент в кредите проводки – ФЛ.*/
  private def flAmt: DataFrame = {
    spark
      .sql(
        """SELECT o.AccountId, fa.FLAmt, o.CutoffDt
        FROM operations o
        LEFT JOIN
              (
                  SELECT AccountDB, sum(Amount) as FLAmt, CutoffDt
                  FROM operation
                  INNER JOIN
                      (
                          SELECT AccountId FROM account
                          WHERE clientId in (SELECT ClientId FROM client WHERE Type = "Ф")
                      )
                  ON AccountDB = AccountId
                  GROUP BY AccountDB, CutoffDt
              ) fa
        ON o.AccountId = fa.AccountDB and o.CutoffDt = fa.CutoffDt"""
      )
  }
}