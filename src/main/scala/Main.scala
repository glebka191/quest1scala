import java.io.{FileNotFoundException, IOException}

import logger.MyLogger
import wr.ReadWritePG

//логгер, исключения и пути убрать в проперти

object Main {
  def main(args: Array[String]): Unit = {
    try {
    val looger = new MyLogger(args(4))
      val reader = new ReadWritePG;
      reader.read(args(0), args(1), args(2), args(3),args(4), args(5))
      looger.logInfo("Чтение аргументов в методе main")
    }catch {
      case e:IllegalArgumentException => println("Не указаны аргументы", e.printStackTrace())
      case fe:FileNotFoundException => println("Не удается найти путь к файлу", fe.printStackTrace())
      case ex:IOException => println("Ошибка при чтении logger.config", ex.printStackTrace())

    }
  }
}