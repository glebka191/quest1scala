package showcases

import java.io.IOException
import java.sql.SQLException

import logger.MyLogger
import org.apache.hadoop.security.AccessControlException
import org.apache.spark.sql.{DataFrame, SaveMode}
import org.apache.spark.sql.functions.{col, max, regexp_replace, sum}
import org.apache.spark.sql.types.{DoubleType, FloatType, LongType}
import org.postgresql.util.PSQLException
import wr.{ConnectToDb, WriteLocal}


class SecondShowcase (Operations:DataFrame, Accounts:DataFrame, Clients:DataFrame,
                      Currency:DataFrame, connection : ConnectToDb, logger:MyLogger) {
  def showcase(): Unit = {
    try {
      var corporate_account = Accounts
        .select("ClientId", "AccountId", "AccountNum", "DateOpen")

      corporate_account =
        corporate_account.join(clientName, Seq("ClientId"), "left")

      corporate_account =
        corporate_account.join(cutoffDt, Seq("AccountId"), "left")

      corporate_account = corporate_account
        .join(paymentAmt, paymentAmt("AccountDB") === corporate_account("AccountID"),
          "left")
        .drop("AccountDB", "DateOp", "AccountCR")

      corporate_account = corporate_account
        .join(enrolementAmt, corporate_account("AccountId") === enrolementAmt("AccountCR"),
          "outer")
        .drop("AccountCR", "DateOp", "AccountDB")

      corporate_account = corporate_account
        .withColumn("TotalAmt", col("PaymentAmt") + col("EnrollementAmt"))
        .withColumn(
          "TotalAmt",
          col("TotalAmt").cast(DoubleType))
        .drop("PaymentAmt", "EnrollementAmt")
      logger.logInfo("Сборка corporate_account")

      //Запись локально
      val write = new WriteLocal().writeShowcase(corporate_account,"corporate_account");

      //запись в БД
      corporate_account.write.mode(SaveMode.Overwrite).jdbc(connection.url,
        "corporate_account", connection.pr)
      logger.logInfo("Успешная запись в бд corporate_account")


      /*
      //запись в hdfs
      corporate_account.repartition(1)
        .write
        .format("parquet")
        .mode(SaveMode.Overwrite)
        .parquet("hdfs://192.168.56.1:8020/user/root/gantosik/corporate_account")
      logger.logInfo("запись corporate_account в HDFS")

       */

    }catch {
      case e:SQLException => logger.logError("Ошибка при сборке corporate_account", e)
      case io:PSQLException => logger.logError("Ошибка при записи corporate_account", io)
      case ex:AccessControlException => logger.logError("Ошибка доступа к HDFS", ex)
    }
  }

  def operations: DataFrame = {
    val operations =
      operationCR
        .union(operationDB)
        .withColumn("AccountId", col("AccountCR"))
        .withColumnRenamed("DateOp", "CutoffDt")
        .drop("AccountCR", "Amount", "Currency", "Comment")
        .distinct()
    operations
  }

  def enrolementAmt: DataFrame = {
    val enrolementAmt =
      operationDF
        .select("AccountCR", "Amount", "DateOp")
        .groupBy("AccountCR","DateOp")
        .agg(sum(col("Amount")).as("EnrollementAmt"))
        .withColumn(
          "EnrollementAmt",
          col("EnrollementAmt").cast(DoubleType) / 1000
        )
    enrolementAmt
  }

  def paymentAmt: DataFrame = {
    val paymentAmt =
      operationDF
        .select("AccountDB", "Amount", "DateOp")
        .groupBy("AccountDB", "DateOp")
        .agg(sum(col("Amount")).as("PaymentAmt"))
        .withColumn("PaymentAmt", col("PaymentAmt").cast(DoubleType) / 1000)
    paymentAmt
  }

  def operationDF: DataFrame = {
    val operation = Operations
      .withColumn(
        "Amount",
        (regexp_replace(col("Amount"), ",", ".").cast(DoubleType) * 10).cast(
          LongType
        )
      )
      .join(actualRate, Seq("Currency"), "inner")
      .withColumn("Amount", col("Amount") * col("Rate"))
      .drop("Currency", "Rate")
    operation
  }

  def cutoffDt: DataFrame = {
    val cutoffDt = operationDB
      .withColumnRenamed("AccountDB","AccountId")
      .drop("AccountCR", "Amount", "Currency", "Comment")
    cutoffDt
  }

  def actualRate: DataFrame = {
    val operationDF = Operations.select("Currency")
    val rateDF = Currency
      .select("RateDate", "Currency", "Rate")
      .withColumn(
        "Rate",
        regexp_replace(col("Rate"), ",", ".").cast(FloatType)
      )
      .withColumn("Rate", col("Rate") * 100)
      .withColumn("Rate", col("Rate").cast(LongType))

    val lastDateOnRate = rateDF.agg(max("RateDate"))
    val actualRate = operationDF
      .join(
        rateDF,
        Seq("Currency"),
        "inner"
      )
      .where(rateDF("RateDate") === lastDateOnRate.head().apply(0))
      .drop("RateDate")
      .distinct()
    actualRate
  }

  def operationCR: DataFrame = {
    val operationDB = Operations
        .select("")
      .withColumnRenamed("DateOp", "CutoffDt")
      .orderBy("AccountCR")
    operationDB
  }

  def operationDB: DataFrame = {
    val operationDB = Operations
      .withColumnRenamed("DateOp", "CutoffDt")
      .orderBy("AccountDB")
    operationDB
  }


  def clientName: DataFrame = {
    val clientName = Clients
      .drop("Type", "Form", "RegisterDate")
      .select("ClientId", "ClientName")
    clientName
  }
}