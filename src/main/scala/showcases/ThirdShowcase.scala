package showcases

import java.io.IOException
import java.sql.SQLException

import logger.MyLogger
import org.apache.hadoop.security.AccessControlException
import org.apache.spark.sql.{DataFrame, SaveMode}
import org.apache.spark.sql.functions.{col, max, regexp_replace, sum}
import org.apache.spark.sql.types.{DoubleType, FloatType, LongType}
import org.postgresql.util.PSQLException
import wr.{ConnectToDb, WriteLocal}




class ThirdShowcase (Operations:DataFrame, Accounts:DataFrame, Clients:DataFrame,
                     Currency:DataFrame, connection:ConnectToDb, logger:MyLogger) {

  def showcase(): Unit = {
    try {
      var corporate_info = Accounts
        .select("ClientId", "DateOpen")
        .distinct()

      corporate_info = corporate_info.join(clientType, Seq("ClientId"), "left")

      corporate_info =
        corporate_info.join(cutoffDt, Seq("ClientId"), "left")

      corporate_info = corporate_info
        .join(paymentAmt, paymentAmt("AccountDB") === corporate_info("ClientId"),
          "left")
        .drop("AccountDB", "DateOp", "AccountCR")

      corporate_info = corporate_info
        .join(enrolementAmt, corporate_info("ClientId") === enrolementAmt("AccountCR"),
          "left")
        .drop("AccountCR", "DateOp", "AccountDB")

      corporate_info = corporate_info
        .withColumn("TotalAmt", col("PaymentAmt") + col("EnrollementAmt"))
        .drop("PaymentAmt", "EnrollementAmt")
      logger.logInfo("Сборка corporate_info")


      //запись локально
      val write = new WriteLocal().writeShowcase(corporate_info,"corporate_info");



          //запись в HDFS
      corporate_info.repartition(1)
        .write
        .format("parquet")
        .mode(SaveMode.Overwrite)
        .parquet("hdfs://192.168.56.1:8020/user/root/gantosik/corporate_info")

      logger.logInfo("Запись corporate_info в HDFS")



      //запись в БД
      corporate_info.write.mode(SaveMode.Overwrite).jdbc(connection.url,
       "corporate_info", connection.pr)
      logger.logInfo("Успешная запись в бд corporate_info")

    }catch {
      case e:SQLException => logger.logError("Ошибка при сборке corporate_info", e)
      case io:PSQLException => logger.logError("Ошибка при записи corporate_info", io)
      case ex:AccessControlException => logger.logError("Ошибка доступа к HDFS", ex)
    }
  }

  def clientType: DataFrame = {
    val clientType = Clients
        .select("ClientId", "ClientName", "Type", "Form", "RegisterDate")
    clientType
  }

  def operations: DataFrame = {
    val operations =
      operationCR
        .union(operationDB)
        .withColumn("AccountId", col("AccountCR"))
        .withColumnRenamed("DateOp", "CutoffDt")
        .drop("AccountCR", "Amount", "Currency", "Comment")
        .distinct()
    operations
  }

  def enrolementAmt: DataFrame = {
    val enrolementAmt =
      operationDF
        .select("AccountCR", "Amount", "DateOp")
        .groupBy("AccountCR","DateOp")
        .agg(sum(col("Amount")).as("EnrollementAmt"))
        .withColumn(
          "EnrollementAmt",
          col("EnrollementAmt").cast(DoubleType) / 1000
        )
    enrolementAmt
  }

  def paymentAmt: DataFrame = {
    val paymentAmt =
      operationDF
        .select("AccountDB", "Amount", "DateOp")
        .groupBy("AccountDB", "DateOp")
        .agg(sum(col("Amount")).as("PaymentAmt"))
        .withColumn("PaymentAmt", col("PaymentAmt").cast(DoubleType) / 1000)
    paymentAmt
  }

  def operationDF: DataFrame = {
    val operation = Operations
      .withColumn(
        "Amount",
        (regexp_replace(col("Amount"), ",", ".").cast(DoubleType) * 10).cast(
          LongType
        )
      )
      .join(actualRate, Seq("Currency"), "inner")
      .withColumn("Amount", col("Amount") * col("Rate"))
      .drop("Currency", "Rate")
    operation
  }

  def actualRate: DataFrame = {
    val operationDF = Operations.select("Currency")
    val rateDF = Currency
      .select("RateDate", "Currency", "Rate")
      .withColumn(
        "Rate",
        regexp_replace(col("Rate"), ",", ".").cast(FloatType)
      )
      .withColumn("Rate", col("Rate") * 100)
      .withColumn("Rate", col("Rate").cast(LongType))

    val lastDateOnRate = rateDF.agg(max("RateDate"))
    val actualRate = operationDF
      .join(
        rateDF,
        Seq("Currency"),
        "inner"
      )
      .where(rateDF("RateDate") === lastDateOnRate.head().apply(0))
      .drop("RateDate")
      .distinct()
    actualRate
  }

  def operationCR: DataFrame = {
    val operationDB = Operations
      .withColumnRenamed("DateOp", "CutoffDt")
      .orderBy("AccountCR")
    operationDB
  }

  def operationDB: DataFrame = {
    val operationDB = Operations
      .withColumnRenamed("DateOp", "CutoffDt")
      .orderBy("AccountDB")
    operationDB
  }

  def cutoffDt: DataFrame = {
    val cutoffDt = operationDB
      .withColumnRenamed("AccountDB","ClientId")
      .drop("AccountCR", "Amount", "Currency", "Comment")
    cutoffDt
  }
}