package showcases

import org.apache.spark.sql.{DataFrame, SparkSession}

class CalculationParams (spark:SparkSession){
  val path = "src\\main\\resource\\calculation_params\\calculation_params_tech.csv"

  val calculationParams = spark
    .read
    .option("delimiter", ";")
    .option("header", "true")
    .option("encoding", "Windows-1251")
    .csv(path)

  def getWordsMasks(masks: DataFrame, index: Int): Array[String] = {
    var words: Array[String] =
      masks
        .select(masks.columns.toList(index))
        .collect
        .map(row => row.getString(0))

    words = words.toStream
      .filter(x => x != null)
      .map(x => s"""${x.trim}""")
      .toArray
    words
  }
}