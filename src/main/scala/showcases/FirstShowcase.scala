package showcases

import java.io.IOException
import java.sql.SQLException

import logger.MyLogger
import org.apache.hadoop.security.AccessControlException
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.apache.spark.sql.functions.{col, max, regexp_replace, sum}
import org.apache.spark.sql.types.{DoubleType, FloatType, IntegerType, LongType}
import org.postgresql.util.PSQLException
import wr._


class FirstShowcase (spark:SparkSession, Operations:DataFrame, Accounts:DataFrame,
                     Clients:DataFrame, Currency:DataFrame, connection : ConnectToDb,logger:MyLogger) {

  val calcparamDf = new CalculationParams(spark).calculationParams

  def showcase(): Unit = {
    try {
      var operation_payments = Accounts
        .select("ClientId", "AccountId")

      operation_payments =
        operation_payments.join(operations, Seq("AccountId"), "left")

      operation_payments = operation_payments
        .join(paymentAmt, paymentAmt("AccountDB") === operation_payments("AccountID")
          && paymentAmt("DateOp") === operation_payments("CutoffDt"),
          "left")
        .drop("AccountDB", "DateOp")

      operation_payments = operation_payments
        .join(enrolementAmt, operation_payments("AccountId") === enrolementAmt("AccountCR")
          && enrolementAmt("DateOp") === operation_payments("CutoffDt"),
          "outer"
        )
        .drop("AccountCR", "DateOp")

      operation_payments = operation_payments
        .join(taxAmtDF, Seq("AccountId", "CutoffDt"), "left")
        .drop("AccountDB", "DateOp")

      operation_payments = operation_payments
        .join(
          clearAmt,
          clearAmt("AccountCR") === operation_payments("AccountId") && clearAmt(
            "DateOp"
          ) === operation_payments("CutoffDt"),
          "left"
        )
        .drop("AccountCR", "DateOp")

      operation_payments = operation_payments
        .join(carsAmtDF, Seq("AccountId", "CutoffDt"), "left")

      operation_payments = operation_payments
        .join(foodAmtDF, Seq("AccountId", "CutoffDt"), "left")

      operation_payments = operation_payments
        .join(
          FLAtm,
          FLAtm("AccountDB") === operation_payments("AccountID") &&
            FLAtm("DateOP") === operation_payments("CutoffDt"),
          "left"
        )
        .drop("AccountDB", "DateOp")

      operation_payments = operation_payments
        .select(
          "AccountId",
          "ClientId",
          "PaymentAmt",
          "EnrollementAmt",
          "TaxAmt",
          "ClearAmt",
          "CarsAmt",
          "FoodAmt",
          "FLAtm",
          "CutoffDt"
        )
        .withColumn("AccountId", col("AccountId").cast(IntegerType))
        .orderBy("AccountId", "CutoffDt")
      logger.logInfo("Сборка operation_payments")


      //запись в HDFS


      operation_payments.repartition(1)
        .write
        .format("parquet")
        .mode(SaveMode.Overwrite)
        .parquet("hdfs://192.168.56.1:8020/user/root/operations_payments")
      logger.logInfo("запись operation_payments в HDFS")
      //192.168.1.10:8020



        //запись витрины локально
//      val write = new WriteLocal().writeShowcase(operation_payments,"operation_payments");


      //Запись в БД
      /*
      operation_payments.write.mode(SaveMode.Overwrite).jdbc(connection.url,
        "operation_payments", connection.pr)
      logger.logInfo("Успешная запись в бд operation_payments")
       */

    }catch {
      case e:SQLException => logger.logError("Ошибка при сборке operation_payments", e)
      case io:PSQLException => logger.logError("Ошибка при записи operation_payments", io)
      case ex:AccessControlException => logger.logError("Ошибка доступа к HDFS", ex)
    }
  }

  def paymentAmt: DataFrame = {
        val paymentAmt =
          operationDF
            .select("AccountDB", "Amount", "DateOp")
            .groupBy("AccountDB", "DateOp")
            .agg(sum(col("Amount")).as("PaymentAmt"))
            .withColumn("PaymentAmt", col("PaymentAmt").cast(DoubleType) / 1000)
        paymentAmt
      }

  def enrolementAmt: DataFrame = {
        val enrolementAmt =
          operationDF
            .select("AccountCR", "Amount", "DateOp")
            .groupBy("AccountCR", "DateOp")
            .agg(sum(col("Amount")).as("EnrollementAmt"))
            .withColumn(
              "EnrollementAmt",
              col("EnrollementAmt").cast(DoubleType) / 1000
            )
        enrolementAmt
      }

  //    TaxAmt – сумму операций, где счет клиента указан в дебете, и счет кредита 40702 (AccountNum)
  def taxAmtDF: DataFrame = {
    val accountNum = accountDF
      .select("AccountId", "AccountNum")
      .where(col("AccountNum").like("40702%"))
      .drop("AccountNum")

    operationDF
      .drop("Comment")
      .join(accountNum, col("AccountCR") === col("AccountId"), "inner")
      .drop("AccountCR", "AccountId")
      .groupBy("AccountDB", "DateOp")
      .agg(sum(col("Amount")).as("TaxAmt"))
      .withColumnRenamed("AccountDB", "AccountId")
      .withColumnRenamed("DateOp", "CutoffDt")
  }

  /*
  def taxAmt: DataFrame = {
        var taxAmt = Accounts
          .select("AccountId", "AccountNum")
          .withColumn("AccountNum", col("AccountNum").substr(1, 5))
        val oper = operationDF.select("AccountDB", "Amount", "DateOp")

        taxAmt = taxAmt
          .join(oper, taxAmt("AccountId") === oper("AccountDB"), "left")
          .drop("AccountId")

        taxAmt = taxAmt
          .where(col("AccountNum") === "40702")
          .groupBy("AccountDB", "AccountNum", "DateOp")
          .agg(sum(col("Amount")).as("TaxAmt"))
          .drop("AccountNum")
          .withColumn("TaxAmt", col("TaxAmt").cast(DoubleType) / 1000)
        taxAmt
      }
   */

  def clearAmt: DataFrame = {
        var clearAmt = Accounts
          .select("AccountId", "AccountNum")
          .withColumn("AccountNum", col("AccountNum").substr(1, 5))
        val oper = operationDF.select("AccountCR", "Amount", "DateOp")

        clearAmt = clearAmt
          .join(oper, clearAmt("AccountId") === oper("AccountCR"), "left")
          .drop("AccountId")

        clearAmt = clearAmt
          .where(col("AccountNum") === "40802")
          .groupBy("AccountCR", "AccountNum", "DateOp")
          .agg(sum(col("Amount")).as("ClearAmt"))
          .drop("AccountNum")
          .withColumn("ClearAmt", col("ClearAmt").cast(DoubleType) / 1000)
        clearAmt
      }
/*
  private def carsAmt: DataFrame = {
    val filterCarsWords: String = s"""Comment NOT like "
                                  ${new CalculationParams(spark).getWordsMasks(calcparamDf,0)
      .mkString(
        """" and Comment NOT like """"
      )}""""
    spark
      .sql(
        s"""
          SELECT o.AccountId, ca.CarsAmt, o.CutoffDt
          FROM operations o LEFT JOIN
          (SELECT AccountDB as AccountId, sum(Amount) as CarsAmt, CutoffDt
            FROM operation
            WHERE ${filterCarsWords}
            GROUP BY AccountId, CutoffDt
          ) ca
        ON o.AccountId = ca.AccountId and o.CutoffDt = ca.CutoffDt"""
      )
  }

 */

  private def carsAmtDF: DataFrame = {
    val carsWords: Array[String] = new CalculationParams(spark).getWordsMasks(calcparamDf, 0)

    val lineFilter: String = s"""Comment NOT like '${carsWords.mkString(
      """' and Comment NOT like '"""
    )}'"""

    var carsAmt: DataFrame =
      operationDF
        .where(s"""$lineFilter""")
        .select("AccountDB", "DateOp", "Amount")

    carsAmt = carsAmt
      .groupBy("AccountDB", "DateOp")
      .agg(sum("Amount").as("CarsAmt"))
      .withColumnRenamed("AccountDB", "AccountId")
      .withColumnRenamed("DateOp", "CutoffDt")
    .withColumn("CarsAmt", col("CarsAmt").cast(DoubleType) / 1000)
    carsAmt
  }


  private def foodAmtDF: DataFrame = {
    val foodWords: Array[String] = new CalculationParams(spark).getWordsMasks(calcparamDf, 1)
    val lineFilter: String = s"""Comment like '${foodWords.mkString(
      """' or Comment like '"""
    )}'"""

    var foodAmt: DataFrame =
      operationDF
        .where(s"""$lineFilter""")
        .select("AccountCR", "Amount", "DateOp")

    foodAmt = foodAmt
      .groupBy("DateOp", "AccountCR")
      .agg(sum("Amount").as("FoodAmt"))
      .withColumnRenamed("AccountCR", "AccountId")
      .withColumnRenamed("DateOp", "CutoffDt")
    .withColumn("FoodAmt", col("FoodAmt").cast(DoubleType) / 1000)
    foodAmt
  }

  def FLAtm: DataFrame = {
        val clientInd =
          operationDF
            .drop("AccountDB", "Comment")
            .join(
              clientDF
                .select("ClientId", "Type")
                .where(col("Type") === "Ф"),
              operationDF("AccountCR") === clientDF("ClientId"),
              "left"
            )
            .drop("AccountCR", "DateOp", "Amount", "Type")
            .distinct()
        var flatm = operationDF
          .drop("AccountCR", "Comment")
          .groupBy("AccountDB", "DateOp")
          .agg((sum(col("Amount")).cast(DoubleType) / 1000).as("FLAtm"))
          .drop("ClientId")
        flatm = flatm
          .join(
            clientInd,
            flatm("AccountDB") === clientInd("ClientId"),
            "inner"
          )
          .drop("ClientId")
        flatm
      }

  def getWords(line: String): Array[String] = {
        val words: Array[String] = line
          .split(",")
        words.foreach(word => word.trim)
        words
      }

  def operationDF: DataFrame = {
        val operation = Operations
          .withColumn(
            "Amount",
            (regexp_replace(col("Amount"), ",", ".").cast(DoubleType) * 10).cast(
              LongType
            )
          )
          .join(actualRate, Seq("Currency"), "inner")
          .withColumn("Amount", col("Amount") * col("Rate"))
          .drop("Currency", "Rate")
        operation
      }

  def operationDB: DataFrame = {
        val operationDB = Operations
          .drop("AccountCR")
          .orderBy("AccountDB")
        operationDB
      }

  def operationCR: DataFrame = {
        val operationDB = Operations
          .drop("AccountDB")
          .orderBy("AccountCR")
        operationDB
      }

  def clientDF: DataFrame = {
        val clientDF = Clients
        clientDF
      }

  def accountDF: DataFrame = {
        Accounts
      }

  def operations: DataFrame = {
        val operations =
          operationCR
            .union(operationDB)
            .withColumn("AccountId", col("AccountCR"))
            .withColumnRenamed("DateOp", "CutoffDt")
            .drop("AccountCR", "Amount", "Currency", "Comment")
            .distinct()
        operations
      }

   def actualRate: DataFrame = {
        val operationDF = Operations.select("Currency")
        val rateDF = Currency
          .select("RateDate", "Currency", "Rate")
          .withColumn(
            "Rate",
            regexp_replace(col("Rate"), ",", ".").cast(FloatType)
          )
          .withColumn("Rate", col("Rate") * 100)
          .withColumn("Rate", col("Rate").cast(LongType))

        val lastDateOnRate = rateDF.agg(max("RateDate"))
        val actualRate = operationDF
          .join(
            rateDF,
            Seq("Currency"),
            "inner"
          )
          .where(rateDF("RateDate") === lastDateOnRate.head().apply(0))
          .drop("RateDate")
          .distinct()
        actualRate
   }
}