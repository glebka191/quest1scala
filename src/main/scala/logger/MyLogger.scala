package logger

import java.io.{BufferedWriter, OutputStreamWriter, _}
import java.time.LocalDateTime
import java.util.Properties

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FSDataOutputStream, FileSystem, Path}
import org.apache.hadoop.security.AccessControlException
import org.apache.hadoop.util.Progressable

class MyLogger (path:String){

 // val in = getClass.getResourceAsStream(path)
  val properties = new Properties()
  properties.load(new FileInputStream(path))

    val status = properties.getProperty("status")
    val pathResult = properties.getProperty("path")
    val writeToHdfs = properties.getProperty("hdfs")
    val hdfsUrl = properties.getProperty("hdfsURL")

  def logInfo(event: String): Unit = {
    if (status.equals("all") || status.equals("Info")) {
      try {
        val logFile = new FileWriter(pathResult, true)
        val bufferWrite = new BufferedWriter(logFile)
        bufferWrite.write("\n" + LocalDateTime.now() + " Info " + event)
        bufferWrite.close()
      } catch {
        case e: FileNotFoundException => println("Указанный logFile отсутвует!")
        case io: IOException => println("Ошибка записи logFile")
      }
    }
  }

  def logError(event:String, exception: Exception): Unit ={
    if (status.equals("all") || status.equals("Error")) {
      try {
      val logFile = new FileWriter(pathResult,true)
      val bufferWrite = new BufferedWriter(logFile)
      bufferWrite.write("\n" + LocalDateTime.now() + " Error " + event + " " + exception)
      bufferWrite.close()
    } catch {
      case e: FileNotFoundException => println("Указанный logFile отсутвует!")
      case io: IOException => println("Ошибка записи logFile")
    }
    }
  }

  def wrToHdfs(): Unit ={
    try {
      val file = new File(pathResult)
      val input = new FileInputStream(file)
      val path = new Path("log.txt")
      val bytes = new Array[Byte](128)

      val conf = new Configuration()
      conf.set("fs.defaultFS", hdfsUrl)
      val fs = FileSystem.get(conf)
      val os = fs.create(path)
      while ((input.read(bytes)) != -1) {
        os.write(bytes)
      }
      fs.close()

    } catch{
      case e:IOException => println("Ошибка записи файла", e)
      case ex:AccessControlException => println("Ошибка доступа к HDFS", ex)
    }
  }
}