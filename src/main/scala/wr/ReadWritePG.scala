package wr

import logger.MyLogger
import org.apache.spark.SparkContext
import org.apache.spark.sql.{SaveMode, SparkSession}
import org.postgresql.util.PSQLException
import showcases.{CalculationParams, FirstShowcase, SecondShowcase, ThirdShowcase}


class ReadWritePG(){
  def read(client:String, accounts:String, operations:String, rate:String, DbProperties:String,
           pathJson:String) ={
    val sc = new SparkContext("local[*]", "Spark")
    val spark = SparkSession.builder
      .appName("appName")
      .getOrCreate()

    val logger = new MyLogger(DbProperties)
    val connection = new ConnectToDb(pathJson)
    val calc = new CalculationParams(spark).calculationParams
    calc.show()

    try {
    val clientDf = spark
      .read
      .option("delimiter", ";")
      .option("header", "true")
      .option("encoding", "Windows-1251")
      .csv(client)
    logger.logInfo("Чтение из csv  в clientDf")
//    clientDf.write.mode(SaveMode.Overwrite).jdbc(connection.url,
//      "clientDF", connection.pr)


    val accountsDf = spark.read
      .option("delimiter", ";")
      .option("header", "true")
      .option("encoding", "Windows-1251")
      .csv(accounts)
    logger.logInfo("Чтение из csv  в accountsDf")
//    accountsDf.write.mode(SaveMode.Overwrite).jdbc(connection.url,
//      "accountsDf",connection.pr)


    val operationsDf = spark.read
      .option("delimiter", ";")
      .option("header", "true")
      .option("encoding", "Windows-1251")
      .csv(operations)
    logger.logInfo("Чтение из csv  в operationsDf")
//    operationsDf.write.mode(SaveMode.Overwrite).jdbc(connection.url,
//      "operationsDf",connection.pr)


    val rateDf = spark.read
      .option("delimiter", ";")
      .option("header", "true")
      .option("encoding", "Windows-1251")
      .csv(rate)
    logger.logInfo("Чтение из csv  в rateDf")
//    rateDf.write.mode(SaveMode.Overwrite).jdbc(connection.url,
//      "rateDf",connection.pr)


    //val write = new WriteLocal().write(clientDf,accountsDf,operationsDf,rateDf, logger);
    val firstShowcase = new FirstShowcase(spark,operationsDf,accountsDf,clientDf,rateDf, connection, logger).showcase();
    //val secondShowcase = new SecondShowcase(operationsDf, accountsDf,clientDf,rateDf, connection, logger).showcase();
    //val thirdShowcase = new ThirdShowcase(operationsDf, accountsDf,clientDf,rateDf, connection, logger).showcase();


  //logger.wrToHdfs()
  }catch {
    case e:PSQLException => println("Ошибка записи DataFrame в БД.")
      logger.logError("Ошибка записи DF", e)
  }
  }
}