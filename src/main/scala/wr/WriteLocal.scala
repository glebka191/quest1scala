package wr

import java.io.IOException

import logger.MyLogger
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.{DataFrame, SaveMode}

class WriteLocal {
  var path = "C:\\Users\\gantosik\\IdeaProjects\\Quest1Scala-master\\src\\main\\output\\"
  var pathShowcases = "C:\\Users\\gantosik\\IdeaProjects\\Quest1Scala-master\\src\\result\\"

  def write(clientDf:DataFrame, accountsDf:DataFrame, operationsDf:DataFrame, rateDf:DataFrame, logger:MyLogger) {
    try {
      val rClientDf = clientDf
        .repartition(col("RegisterDate"))
        .write.format("parquet")
        .partitionBy("RegisterDate")
        .mode(SaveMode.Overwrite) parquet (path + "client.parquet")
    } catch {
      case e: IOException => println("Ошибка записи clientDf")
        logger.logError("Ошибка записи clientDf", e)
    }

    try {
      val rAccountDF = accountsDf
        .repartition(col("DateOpen"))
        .write.format("parquet")
        .partitionBy("DateOpen")
        .mode(SaveMode.Overwrite) parquet (path + "accounts.parquet")
    } catch {
      case e: IOException => println("Ошибка записи accountsDf")
        logger.logError("Ошибка записи accountsDf", e)
    }

    try {
      val rOperationsDf = operationsDf
        .repartition(col("DateOp"))
        .write.format("parquet")
        .partitionBy("DateOp")
        .mode(SaveMode.Overwrite) parquet (path + "operations.parquet")
    } catch {
      case e: IOException => println("Ошибка записи operationsDf")
        logger.logError("Ошибка записи operationsDf", e)
    }
    try {
      val rRateDf = rateDf
        .repartition(col("RateDate"))
        .write.format("parquet")
        .partitionBy("RateDate")
        .mode(SaveMode.Overwrite) parquet (path + "rate.parquet")
    }catch {
      case e: IOException => println("Ошибка записи rateDf")
        logger.logError("Ошибка записи rateDf", e)
    }
  }

  def writeShowcase(showCase : DataFrame, name : String): Unit ={
    val showcase = showCase
      .coalesce(1)
      .write
      .mode("overwrite")
      .format("com.databricks.spark.csv")
      .option("delimiter", ";")
      .option("header", "true")
      .option("encoding", "Windows-1251")
      .save(pathShowcases + name)



    //val showcase = showCase
    //  .repartition(1)
    //  .write.format("com.databricks.spark.csv")
    //  .partitionBy("CutoffDt")
    //  .mode(SaveMode.Overwrite) csv(pathShowcases + name)
   }
}