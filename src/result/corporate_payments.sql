with rate as (SELECT "Currency", "Rate", "RateDate"
                       FROM ratedf
                       WHERE "RateDate" = (SELECT max("RateDate") from ratedf)),
     payment_amt as (
         select "AccountDB","DateOp", sum("Amount"*"Rate") as "PaymentAmt"
         from operationsdf op join rate rr
                                      on op."Currency"=rr."Currency"
         group by "AccountDB", "DateOp"),
     enrolment_amt as(
         select  "AccountCR","DateOp", sum("Amount"*"Rate") as "EmrolmentAmt"
         from operationsdf op left join rate rr
                                           on op."Currency"=rr."Currency"
         group by "AccountCR", "DateOp"
     ),
     tax_amt as (
         select "AccountDB", "DateOp", sum("Amount" * "Rate") as "TaxAmt"
         from operationsdf op
                  left join accountsdf ac
                            on  op."AccountCR"=ac."AccountID"
                  left join rate rr
                            on op."Currency"=rr."Currency"
         where ac."AccountNum" like '40702%' group by "AccountDB", "DateOp"),
     clear_amt as (
         select "AccountCR", "DateOp", sum("Amount" * "Rate") as "ClearAmt"
         from operationsdf op
                  left join accountsdf ac
                            on  op."AccountDB"=ac."AccountID"
                  left join rate rr
                            on op."Currency"=rr."Currency"
         where ac."AccountNum" like '40802%' group by "AccountCR", "DateOp" order by "AccountCR"
     ),
     fl_amt as (
         select "AccountDB", "DateOp", sum("Amount" * "Rate") as "FLAmt"
         from operationsdf op
                  left join rate rr
                            on op."Currency"=rr."Currency"
                  left join clientdf cl
                            on "AccountCR" = cl."ClientId"
         where cl."Type"='Ф'
         group by "AccountDB", "DateOp" order by "AccountDB"
     ),
     corporate_payments as (
         select "AccountDB" as "AccountID", "DateOp" as "CutoffDT" from operationsdf op
         UNION
         select "AccountCR" as "AccountID", "DateOp" as "CutoffDT" from operationsdf op order by "AccountID"
     ),
     cars_amt as (
         select "AccountDB", "DateOp", sum("Amount" * "Rate") as "CarsAmt"
         from operationsdf op
                  left join rate rr
                            on op."Currency"=rr."Currency"
         where "Comment" not like '%а/м%, %а\м%, %автомобиль %, %автомобили %, %транспорт%, %трансп%средс%, ' ||
                                  '%легков%, %тягач%, %вин%, %vin%,%viн:%, %fоrd%, %форд%,%кiа%, %кия%, ' ||
                                  '%киа%%мiтsuвisнi%, %мицубиси%, %нissан%, %ниссан%, %sсанiа%, %вмw%, %бмв%, ' ||
                                  '%аudi%, %ауди%, %jеер%, %джип%, %vоlvо%, %вольво%, %тоyота%, %тойота%, ' ||
                                  '%тоиота%, %нyuнdаi%, %хендай%, %rенаulт%, %рено%, %реugеот%, %пежо%, %lаdа%, ' ||
                                  '%лада%, %dатsuн%, %додж%, %меrсеdеs%,' ||
                                  ' %мерседес%, %vоlкswаgен%, %фольксваген%, %sкоdа%, %шкода%, %самосвал%, ' ||
                                  '%rover%, %ровер%'  escape '\'
         group by "AccountDB", "DateOp"
     ),
     food_amt as(
         select "AccountCR", "DateOp", sum("Amount" * "Rate") as "FoodAmt"
         from operationsdf op
                  left join rate rr
                            on op."Currency"=rr."Currency"
         where "Comment" like '% сою%, %соя%, %зерно%, %кукуруз%, %масло%, %молок%, %молоч%, %мясн%,' ||
                              ' %мясо%, %овощ%, %подсолн%, %пшениц%, %рис%, %с/х%прод%, %с/х%товар%,' ||
                              ' %с\х%прод%, %с\х%товар%, %сахар%, %сельск%прод%, %сельск%товар%, %сельхоз%прод%,' ||
                              ' %сельхоз%товар%, %семен%, %семечк%, %сено%, %соев%, %фрукт%, %яиц%, %ячмен%,' ||
                              ' %картоф%, %томат%, %говя%, %свин%, %курин%, %куриц%, %рыб%, %алко%, %чаи%, %кофе%,' ||
                              ' %чипс%, %напит%, %бакале%, %конфет%, %колбас%, %морож%, %с/м%, %с\м%, %консерв%,' ||
                              ' %пищев%, %питан%, %сыр%, %макарон%,' ||
                              ' %лосос%, %треск%, %саир%, % филе%, % хек%, %хлеб%, %какао%, %кондитер%, %пиво%, %ликер%'
         group by "AccountCR", "DateOp"
     )
select cp."AccountID" ,
       (payment_amt."PaymentAmt"),
       enrolment_amt."EmrolmentAmt",
       tax_amt."TaxAmt",
       clear_amt."ClearAmt",
       cars_amt."CarsAmt",
       food_amt."FoodAmt",
       fl_amt."FLAmt",
       cp."CutoffDT" from corporate_payments cp
                              full join payment_amt on "AccountDB" = "AccountID" and "CutoffDT"="DateOp"
                              full join enrolment_amt on "AccountCR" = "AccountID" and "CutoffDT"=enrolment_amt."DateOp"
                              full join tax_amt on tax_amt."AccountDB" = "AccountID" and "CutoffDT"=tax_amt."DateOp"
                              full join clear_amt on clear_amt."AccountCR" = "AccountID" and "CutoffDT"=clear_amt."DateOp"
                              full join fl_amt on fl_amt."AccountDB" = "AccountID" and "CutoffDT"=fl_amt."DateOp"
                              full join cars_amt on cars_amt."AccountDB" = "AccountID" and "CutoffDT"=cars_amt."DateOp"
                              full join food_amt on food_amt."AccountCR" = "AccountID" and "CutoffDT"=food_amt."DateOp"


;