with corp_data as (
    select "AccountId",
           "CutoffDt",
           ("PaymentAmt"+"EnrollementAmt") as "TotalAmt"
    from operation_payments
),
     corporate_account as (
         select  "AccountID",
                 "AccountNum",
                 "DateOpen",
                 c."ClientId",
                 "ClientName"
         from accountsdf
                  left join clientdf c on accountsdf."ClientId" = c."ClientId"
     )
select corporate_account."AccountID",
       "AccountNum",
       "DateOpen",
       "ClientId",
       "ClientName",
       "TotalAmt",
       "CutoffDt"
from corp_data
         left join corporate_account
                   on corp_data."AccountId" = corporate_account."AccountID"
order by corp_data."AccountId";