with account_info as(select "ClientId", "CutoffDt", sum("TotalAmt") as "TotalAmt"
                     from corporate_account
                     group by "ClientId", "CutoffDt"
                     order by "ClientId", "CutoffDt")
select clientdf."ClientId",
       "ClientName",
       "Type",
       "Form",
       "RegisterDate",
       "TotalAmt",
       "CutoffDt"
from account_info
         left join clientdf
                   on account_info."ClientId" = clientdf."ClientId";